# Demo of _LibSVM_

Author : Mr Dk.

Finished On : 2018.6.18

Implemented With : Java

---

#### Directory description

* LibSVM

  ​	The Java project of Demo.

  ​	Including _libsvm.jar_. (Should be added into the build path)

  ​	Including _svm_predict.java_, _svm_train.java_, _svm_toy.java_, _svm_scale.java_.

  ​	Including a Main class.

* DataPretreatment

  ​	A program to make the data set suitable for _LibSVM_.

  ​	Implemented with C/C++.

  ​	Including a _.cpp_ file.

  ​	Including the origin data sets.

---

#### About _LibSVM_

* INDEX : https://www.csie.ntu.edu.tw/~cjlin/libsvm/
* Input format : TAG index1:arg1 index2:arg2 ...... indexn:argn

---

#### Data Set

* MONK's Problem
* A binary classify problem
* URL : http://archive.ics.uci.edu/ml/datasets/MONK%27s+Problems
* Origin format : TAG arg1 arg2 arg3 arg4 arg5 arg6 IDENTITY
* After-pretreatment format : TAG 1:arg1 2:arg2 3:arg3 4:arg4 5:arg5 6:arg6

---

#### Test Result

* __monks-1__ : Accuracy = 81.75519630484989% (354/433) (classification)
* __monks-2__ : Accuracy = 67.6674364896074% (293/433) (classification)
* __monks-3__ : Accuracy = 96.30484988452656% (417/433) (classification)

---



