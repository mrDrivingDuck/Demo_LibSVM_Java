#include <iostream>
#include <fstream>
#include <string>
using namespace std;

#define ARGV 6

int main() {

    FILE *fp = fopen("monks-3.train", "r");
    FILE *fout = fopen("monk-3-train.txt", "w");
    if (!fp) {
        printf("ERROR\n");
        exit(0);
    }

    while (!feof(fp)) {
        char temp[16];
        int tag;
        int argv[ARGV];

        fscanf(fp, "%d", &tag);
        for (int i = 0; i < ARGV; i++) {
            fscanf(fp, "%d", argv+i);
        }
        fscanf(fp, "%s", temp);

        fprintf(fout, "%d", tag);
        for (int i = 0; i < ARGV; i++) {
            fprintf(fout, " %d:", i+1);
            fprintf(fout, "%d", argv[i]);
        }
        fprintf(fout, "\n");
    }

    fclose(fp);
    fclose(fout);
    return 0;
}